import shapeless._
import Nat._

object tree {

  sealed trait T[+A, +N <: Nat, +P <: Nat]
  case class Branch[+A, N <: Nat, P <: Nat, PP <: Nat](node: Node[A, P, PP])(implicit n: PredAux[N, P], p: PredAux[P, PP]) extends T[A, N, P]
  case object Leaf extends T[Nothing, Succ[_0], _0]

  sealed trait Node[+A, N <: Nat, P <: Nat]
  case class T1[+A, N <: Nat, P <: Nat](left: T[A, N, P], a: A, right: T[A, N, P])(implicit n: PredAux[N, P]) extends Node[A, N, P]
  case class T2[+A, N <: Nat, P <: Nat](left: T[A, N, P], a1: A, middle: T[A, N, P], a2: A, right: T[A, N, P])(implicit n: PredAux[N, P]) extends Node[A, N, P]

  trait Tree[+A] {
    type N <: Nat
    type P <: Nat

    def t: T[A, N, P]
  }

  object Tree {
    implicit def treeAux[A, NN <: Nat, PP <: Nat](implicit tt: T[A, NN, PP]) = new Tree[A] {
      type N = NN
      type P = PP
      def t = tt
    }
  }

  type Keep[TT, A, N <: Nat, P <: Nat] = T[A, N, P] => TT
  type Push[TT, A, N <: Nat, P <: Nat] = T[A, N, P] => TT
//
//  def insert[A: Ordering, N <: Nat, P <: Nat, PP <: Nat](x: A, t: Tree[A]) = {
//    def ins
//      b.node match {
//        case T1(l, a, r)         => 
//        if (x == a) T1(l, x, r)
//        else if (x < a) insert(a, l)
//        else if (x > a) insert(a, r)
//        case T2(l, a1, m, a2, r) =>
//    }
//  }
}